// Using require directive to load express module/package
const express = require("express");

// Create an application using express
const app = express();

// The port where the app will listen to 
const port = 3000;

// Allows the server to handle data from request
// Allows the app to read json data
app.use(express.json());

// Allows the app to read the data from forms
app.use(express.urlencoded({extended:true}));

// GET Route 
app.get("/home", (req, res) => {
	res.send("Welcome to the homepage");
}) //they are parameters

let users = [];
// GET Route
app.get("/users", (req,res) => {
	users.push(req.body);
	res.send(users);
})

// DELETE Route
app.delete("/delete-user", (req, res) => {
	res.send(`User ${req.body.username} has been deleted`)
})

// Tells the server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));